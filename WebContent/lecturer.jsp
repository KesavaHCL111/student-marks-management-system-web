<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lecturer</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
	<h1>Lecturer Home Page</h1>
	<p align="right">
		<a class="color" href="lecturerCrud">LecturerCrud</a> &nbsp; <a class="color" href="index.jsp">Logout</a>
	</p>
	<hr />
	<table class="table" border="5">
		<tr>
			<th colspan="7">StudentDetails</th>
		</tr>
		<tr>
			<th>StudentID</th>
			<th>StudentName</th>
			<th>Java</th>
			<th>SQL</th>
			<th>HTML</th>

		</tr>
		<c:forEach items="${student}" var="s">
			<tr>
				<td>${s.getSid()}</td>
				<td>${s.getSname() }</td>
				<td>${s.getJava_Marks()}</td>
				<td>${s.getSql_Marks()}</td>
				<td>${s.getHtml_Marks()}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>