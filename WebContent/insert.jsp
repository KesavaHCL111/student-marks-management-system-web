<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>insert</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<h1>Lecturer Insert Page</h1>
<p align="right">
		 <a class="color" href="index.jsp">Logout</a>
	</p>
	<form id="form" action="add" method="post">

		<table>
			<tr>
				<td>StudentId</td>
				<td><input type="text" name="sid"  /></td>
			</tr>
			<tr>
				<td>StudentName</td>
				<td><input type="text" name="sname" /></td>
			</tr>
			<tr>
				<td>Java</td>
				<td><input type="text" name="java_Marks" /></td>
			</tr>
				<tr>
				<td>SQL</td>
				<td><input type="text" name="sql_Marks"/></td>
			</tr>
				<tr>
				<td>HTML</td>
				<td><input type="text" name="html_Marks" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Add"></td>
			</tr>
		</table>
	</form>

</body>
</html>