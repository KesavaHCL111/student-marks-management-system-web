<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LecturerCrud</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>

	<h1>Lecturer CRUD Operations Page</h1>
	<p align="right">
		<a class="color" href="insert">Add Student</a> &nbsp; <a class="color" href="index.jsp">Logout</a>
	</p>
	<hr />
	<span>${del}</span>
	<br />
	<span>${up}</span>
	<br />
	<span>${ad}</span>
	<br />
	<table class="table" border="8">
		<tr>
			<th colspan="7">StudentDetails</th>
		</tr>
		<tr>
			<th>StudentID</th>
			<th>StudentName</th>
			<th>Java</th>
			<th>SQL</th>
			<th>HTML</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${student}" var="s">
			<tr>
				<td>${s.getSid()}</td>
				<td>${s.getSname() }</td>
				<td>${s.getJava_Marks()}</td>
				<td>${s.getSql_Marks()}</td>
				<td>${s.getHtml_Marks()}</td>
				<td><a style="text-decoration: none;color:blue"
					href="update?sid=${s.getSid()}&sname=${s.getSname()}&java_Marks=${s.getJava_Marks()}&sql_Marks=${s.getSql_Marks()}&html_Marks=${s.getHtml_Marks()}">Update</a></td>
				<td><a style="text-decoration: none; color:blue"
					href="delete?sid=${s.getSid()} ">Delete</a></td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>