<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1"> 

<title>Home</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
	<h1>Welcome to Student Portal</h1>
   <span style="color:#ee1429; font-size:20px;">${error}</span>	
	<form id="form" action="login" method="post">

		<table>
			<tr>
				<td>UserId</td>
				<td><input type="text" name="uid" autocomplete="off"/></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Login"></td>
			</tr>
		</table>
	</form>
</body>
</html>