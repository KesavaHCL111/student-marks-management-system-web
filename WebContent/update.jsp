<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>update</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<h1>Lecturer Update Page</h1>
	<form action="updatecont" method="post">

		<table>
			<tr>
				<td>StudentId</td>
				<td><input type="text" name="sid" value="${u.getSid()}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td>StudentName</td>
				<td><input type="text" name="sname"value="${u.getSname()}" /></td>
			</tr>
			<tr>
				<td>Java</td>
				<td><input type="text" name="java_Marks"value="${u.getJava_Marks() }" /></td>
			</tr>
				<tr>
				<td>SQL</td>
				<td><input type="text" name="sql_Marks"value="${u.getSql_Marks() }" /></td>
			</tr>
				<tr>
				<td>HTML</td>
				<td><input type="text" name="html_Marks"value="${u.getHtml_Marks() }" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>