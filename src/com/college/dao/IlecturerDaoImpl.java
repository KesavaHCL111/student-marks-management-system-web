package com.college.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.college.model.Lecturer;
import com.college.model.StudentMarks;
import com.college.util.Db;
import com.college.util.Query;

public class IlecturerDaoImpl implements ILecturerDao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int lecturerLogin(Lecturer lecturer) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.lecturerLogin);
			pst.setString(1, lecturer.getUid());
			pst.setString(2, lecturer.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in Lecurer Login");
		} finally {
			try {
				pst.close();
				rs.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}

		}

		return result;
	}

	@Override
	public List<StudentMarks> viewAllStudentsMarks() {
		List<StudentMarks> list = new ArrayList<StudentMarks>();
		try {
			pst = Db.getConnection().prepareStatement(Query.viewAllSudentsMarks);
			rs = pst.executeQuery();
			while (rs.next()) {
				StudentMarks marks = new StudentMarks(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4),
						rs.getInt(5));
				list.add(marks);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view all Student marks");
		} finally {

			try {
				rs.close();
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return list;

	}

	@Override
	public void delete(StudentMarks student) {
		try {
			pst=Db.getConnection().prepareStatement(Query.delete);
			pst.setInt(1, student.getSid());
			pst.executeUpdate();
		}catch(SQLException | ClassNotFoundException e){
			System.out.println("Exception occurs in delete");
		}
		
	}

	@Override
	public int edit(StudentMarks student) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.edit);
			pst.setString(1, student.getSname());
			pst.setInt(2, student.getJava_Marks());
			pst.setInt(3, student.getSql_Marks());
			pst.setInt(4, student.getHtml_Marks());
			pst.setInt(5, student.getSid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occurs in edit");
		} finally {

			try {
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}

		return result;
	}

	@Override
	public int add(StudentMarks student) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addStudent);
			pst.setInt(1, student.getSid());
			pst.setString(2, student.getSname());
			pst.setInt(3, student.getJava_Marks());
			pst.setInt(4, student.getSql_Marks());
			pst.setInt(5, student.getHtml_Marks());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in add Student");
		} finally {
			try {
				pst.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}

		}

		return result;
	}



}
