package com.college.dao;

import java.util.List;

import com.college.model.Lecturer;
import com.college.model.StudentMarks;

public interface ILecturerDao {
	public int lecturerLogin(Lecturer lecturer);
	public List<StudentMarks> viewAllStudentsMarks();
	public void delete(StudentMarks student);
	public int edit(StudentMarks student);
	public int add(StudentMarks student);

}
