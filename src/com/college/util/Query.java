package com.college.util;

public class Query {
	public static String lecturerLogin= "select * from lecturer where uid=? and password=?";
	public static String viewAllSudentsMarks="select * from student_marks";
	public static String delete="delete from student_marks where sid=?";
	public static String edit = "update student_marks set sname=?,java_Marks=?,sql_Marks=?,html_Marks=? where sid=?";
	public static String addStudent = "insert into student_marks values(?,?,?,?,?)";
}
