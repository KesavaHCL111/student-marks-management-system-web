package com.college.model;

public class Lecturer {

	private String uid;
	private String password;

	public Lecturer() {
		// TODO Auto-generated constructor stub
	}

	public Lecturer(String uid, String password) {
		super();
		this.uid = uid;
		this.password = password;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
