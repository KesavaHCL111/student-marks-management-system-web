package com.college.model;

public class StudentMarks {

	private Integer sid;
	private String sname;
	private Integer java_Marks;
	private Integer sql_Marks;
	private Integer html_Marks;

	public StudentMarks() {
		// TODO Auto-generated constructor stub
	}

	public StudentMarks(Integer sid, String sname, Integer java_Marks, Integer sql_Marks, Integer html_Marks) {
		super();
		this.sid = sid;
		this.sname = sname;
		this.java_Marks = java_Marks;
		this.sql_Marks = sql_Marks;
		this.html_Marks = html_Marks;
	}

	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Integer getJava_Marks() {
		return java_Marks;
	}

	public void setJava_Marks(Integer java_Marks) {
		this.java_Marks = java_Marks;
	}

	public Integer getSql_Marks() {
		return sql_Marks;
	}

	public void setSql_Marks(Integer sql_Marks) {
		this.sql_Marks = sql_Marks;
	}

	public Integer getHtml_Marks() {
		return html_Marks;
	}

	public void setHtml_Marks(Integer html_Marks) {
		this.html_Marks = html_Marks;
	}

}
