package com.college.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.college.dao.ILecturerDao;
import com.college.dao.IlecturerDaoImpl;
import com.college.model.StudentMarks;

@WebServlet("/delete")
public class DeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int sid = Integer.parseInt(request.getParameter("sid"));
		ILecturerDao dao = new IlecturerDaoImpl();
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		dao.delete(student);
		request.setAttribute("del", sid+" Deleted Successfully");
		request.getRequestDispatcher("lecturerCrud").include(request, response);
	}

}
