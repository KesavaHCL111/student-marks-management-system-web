package com.college.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.college.dao.ILecturerDao;
import com.college.dao.IlecturerDaoImpl;
import com.college.model.Lecturer;
import com.college.model.StudentMarks;

@WebServlet("/login")
public class LecturerLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    String uid=request.getParameter("uid");
	    String password=request.getParameter("password");
	    Lecturer lecturer=new Lecturer(uid,password);
	    ILecturerDao dao=new IlecturerDaoImpl();
	    int result=dao.lecturerLogin(lecturer);
	    if(result>0) {
	    	List<StudentMarks> list=dao.viewAllStudentsMarks();
	    	request.setAttribute("student", list);
	    	request.getRequestDispatcher("lecturer.jsp").forward(request, response);
	    }else {
	    	request.setAttribute("error", "Hi "+uid+"Please check your Credenatials");
			request.getRequestDispatcher("index.jsp").forward(request, response);
	    }
	}

}
