package com.college.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.college.dao.ILecturerDao;
import com.college.dao.IlecturerDaoImpl;
import com.college.model.StudentMarks;

@WebServlet("/add")
public class AddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int sid = Integer.parseInt(request.getParameter("sid"));
		String sname = request.getParameter("sname");
		int java_Marks = Integer.parseInt(request.getParameter("java_Marks"));
		int sql_Marks = Integer.parseInt(request.getParameter("sql_Marks"));
		int html_Marks = Integer.parseInt(request.getParameter("html_Marks"));
		StudentMarks student = new StudentMarks(sid, sname, java_Marks, sql_Marks, html_Marks);
		ILecturerDao dao = new IlecturerDaoImpl();
		int result=dao.add(student);
		if(result>0) {
			request.setAttribute("msg", sid+" Inserted Successfuly");
		}else {
			request.setAttribute("msg", sid+" Duplicate Entry");
		}
		List<StudentMarks> list=dao.viewAllStudentsMarks();
		request.setAttribute("student", list);
		request.setAttribute("ad", sid+" Added Successfully");
		request.getRequestDispatcher("lecturercrud.jsp").forward(request, response);
	}

}
