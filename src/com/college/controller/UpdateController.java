package com.college.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.college.model.StudentMarks;

@WebServlet("/update")
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int sid = Integer.parseInt(request.getParameter("sid"));
		String sname = request.getParameter("sname");
		int java_Marks = Integer.parseInt(request.getParameter("java_Marks"));
		int sql_Marks = Integer.parseInt(request.getParameter("sql_Marks"));
		int html_Marks = Integer.parseInt(request.getParameter("html_Marks"));
		StudentMarks student = new StudentMarks(sid ,sname, java_Marks, sql_Marks, html_Marks);
		request.setAttribute("u", student);
		request.getRequestDispatcher("update.jsp").forward(request, response);
	}

}
