package com.college.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.college.dao.ILecturerDao;
import com.college.dao.IlecturerDaoImpl;
import com.college.model.StudentMarks;

@WebServlet("/lecturerCrud")
public class LecturerCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ILecturerDao dao = new IlecturerDaoImpl();
		List<StudentMarks> list = dao.viewAllStudentsMarks();
		request.setAttribute("student", list);
		request.getRequestDispatcher("lecturercrud.jsp").forward(request, response);
	}

}
